let http = require("http");

let url = process.argv[2];

http.get(url, function (response) {
  //ubah format response data stream dari binary ke utf8
  response.setEncoding("utf8");

  //untuk setiap event bertipe "data", tulis data ke console
  response.on("data", (line) => console.log(line));
});

//funtion;
