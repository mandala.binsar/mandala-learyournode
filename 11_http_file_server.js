const http = require("http");
const fs = require("fs");
const map = require("through2-map");
const bl = require("bl");

let port = process.argv[2];
let dir = process.argv[3];

//console.log(port, " ", dir);

const server = http.createServer((request, response) => {
  let readFile = fs.createReadStream(dir);

  //console.log(fs.readFile());

  //file yang di read dikirim ke response
  readFile.pipe(response);
});

server.listen(port);

/*
  The fs core module also has some streaming APIs for files. You will need  
  to use the fs.createReadStream() method to create a stream representing  
  the file you are given as a command-line argument. The method returns a  
  stream object which you can use src.pipe(dst) to pipe the data from the  
  src stream to the dst stream. In this way you can connect a filesystem  
  stream with an HTTP response stream. 
  */
