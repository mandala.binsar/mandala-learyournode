const http = require("http");
const fs = require("fs");
const map = require("through2-map");
const bl = require("bl");
const url = require("url");

let port = process.argv[2];

//console.log(port, dir);

const server = http.createServer((request, response) => {
  response.writeHead(200, { "Content-Type": "application/json" });

  let endpoint = url.parse(request.url, true);

  //console.log("AAAA", endpoint.search);
  //console.log(endpoint.search.slice(16, 18));
  //console.log(endpoint.search.slice(19, 21));
  //console.log(endpoint.search.slice(22, 24));
  //console.log(endpoint);
  //console.log(endpoint.query.iso);

  let queryDate = endpoint.query.iso;
  let date = new Date(queryDate);
  //console.log(date.getTime());

  if (endpoint.pathname === "/api/parsetime") {
    //console.log(endpoint.query(iso));
    res = {
      hour: date.getHours(),
      minute: date.getMinutes(),
      second: date.getSeconds(),
    };
    response.end(JSON.stringify(res) + "\n");
  }

  if (endpoint.pathname === "/api/unixtime") {
    res = {
      unixtime: date.getTime(),
    };
    response.end(JSON.stringify(res) + "\n");
  }
});

server.listen(port);
