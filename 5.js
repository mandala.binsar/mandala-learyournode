const fs = require("fs");

let dir = process.argv[2];
let ext = process.argv[3];

fs.readdir(dir, function (err, list) {
  if (err) {
    return err;
  } else {
    for (let i = 0; i < list.length; i++) {
      if (list[i].split(".")[1] == ext) {
        console.log(list[i]);
      }
    }
  }
});
