const bl = require("bl");
const http = require("http");

let url = process.argv[2];

http.get(url, function (response) {
  /* ngaco
  let char = 0;
  let full = "";

  response.setEncoding("utf8");
  response.on("data", function (chuck) {});
  */

  /*
  response.pipe(
    bl(function (data) {
      console.log(data);
    })
  );
  */

  response.setEncoding("utf8");
  let res = "";

  //using pipe and bl funtion
  response.pipe(
    bl(function (err, data) {
      console.log(data.length);
      console.log(data.toString());
    })
  );

  //solution using on
  // response.on("data", function (chuck) {
  //   res += chuck;
  // });

  // response.on("end", function () {
  //   console.log(res.length);
  //   console.log(res);
  // });
});

/*
The first line you write should just be an integer representing the number  
of characters received from the server. The second line should contain the  
complete String of characters sent by the server.
*/
