const fs = require("fs");

let fileName = process.argv[2];
let line;

fs.readFile(fileName, function (err, data) {
  if (err) {
    return err;
  } else {
    console.log(data.toString().split("\n").length - 1);
  }
});
