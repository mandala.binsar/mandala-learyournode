const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let input = [];

rl.on("line", (line) => {
  input.push(line);
});

rl.on("close", () => {
  input.reverse();

  while (input.length > 0) {
    let line = input.pop().toLowerCase();
    //line = "Problematic pair programming proble problem".toLowerCase();
    //line = "xxxxxxxxxx".toLowerCase();

    //console.log(line);

    let arr = line.split("problem");

    //console.log(arr.length);

    arr.length > 1 ? console.log("yes") : console.log("no");
  }
});
