console.log("hello world");

setTimeout(function () {
  console.log("hello world 2");

  setTimeout(function () {
    console.log("hello world 3");
  }, 0);
}, 2500);

setTimeout(function () {
  console.log("hello world 4");

  setTimeout(function () {
    console.log("hello world 5");
  }, 2500);
}, 0);

console.log("hello world 6");
