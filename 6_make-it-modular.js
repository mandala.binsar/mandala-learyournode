const mymodule = require("./6_mymodule");

let dir = process.argv[2];
let ext = process.argv[3];

mymodule(dir, ext, function (err, list) {
  if (err) {
    return console.log(err);
  } else {
    list.forEach((element) => console.log(element));
  }
});

/*
console.log(
  mymodule.fileList(dir, process.argv[2], dir, function (err, list) {
    if (err) {
      return err;
    } else {
      for (let i = 0; i < list.length; i++) {
        if (list[i].split(".")[1] == "md") {
          console.log(list[i]);
        }
      }
    }
  })
);
*/
