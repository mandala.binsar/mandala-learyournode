const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let input = [];

rl.on("line", (line) => {
  input.push(line);
});

rl.on("close", () => {
  let num = input[0].split(" ");
  let char = [input[1].charAt(0), input[1].charAt(1), input[1].charAt(2)];
  let order = [];

  num = num.map((n) => parseInt(n));

  let a = Math.min(num[0], num[1], num[2]);
  let c = Math.max(num[0], num[1], num[2]);
  let b = num[0] + num[1] + num[2] - a - c;

  res = "";

  if (char[0] === "A") {
    res = a + " ";
  } else if (char[0] === "B") {
    res = b + " ";
  } else {
    res = c + " ";
  }

  if (char[1] === "A") {
    res += a + " ";
  } else if (char[1] === "B") {
    res += b + " ";
  } else {
    res += c + " ";
  }

  if (char[2] === "A") {
    res += a;
  } else if (char[2] === "B") {
    res += b;
  } else {
    res += c;
  }

  console.log(res);
});

function toNum(char) {
  if ((char = "A")) {
    return 1;
  } else if ((char = "B")) {
    return 2;
  }
  return 3;
}
