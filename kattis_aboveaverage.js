const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let input = [];

rl.on("line", (line) => {
  input.push(line);
});

rl.on("close", () => {
  input.reverse();
  input.pop();

  while (input.length != 0) {
    let line = input.pop().split(" ");
    line.reverse();
    let stu = parseInt(line.pop());

    let total = 0;
    line.forEach((element) => {
      total += parseInt(element);
    });

    let ave = total / stu;
    //console.log(total, stu, total / stu, ave);

    let above = 0;
    line.forEach((element) => {
      if (parseInt(element) > ave) {
        above++;
      }
    });

    //console.log(above, above / stu, ((above * 100) / stu).toFixed(3) + "%");
    console.log(((above * 100) / stu).toFixed(3) + "%");
  }
});
