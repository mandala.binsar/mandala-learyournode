const fs = require("fs");
var path = require("path");

module.exports = function fileList(dir, fileExt, callback) {
  //console.log("A");
  fs.readdir(dir, function (err, list) {
    //console.log("B");
    if (err) {
      //     console.log("C");
      return callback(err, null);
    } else {
      //console.log(fileExt, "D", list.length);
      let validList = [];
      for (let i = 0; i < list.length; i++) {
        //console.log(list[i].split(".")[1] == fileExt);
        if (list[i].split(".")[1] == fileExt) {
          //console.log(list[i]);
          validList.push(list[i]);
        }
      }
      //console.log(validList);
      return callback(null, validList);
    }
  });
};
/*
fs.readdir(dir, function (err, list) {
  if (err) {
    return callback();
  } else {
    for (let i = 0; i < list.length; i++) {
      if (list[i].split(".")[1] == "md") {
        console.log(list[i]);
      }
    }
  }
});
*/
//single function that takes three arguments: the
//directory name, the filename extension string and your callback function,
//in that order. Don't alter the filename extension string in any way before
//passing it to your module.
