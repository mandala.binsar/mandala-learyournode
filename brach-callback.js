//arg = string
// if len < 10 -> callback1(arg)

arg = "SSSSSSSS";

function branch(arg, callback1, callback2) {
  return arg.length < 10 ? callback1(arg) : callback2(arg);
}

const text = {
  a: "somestring",
  b: 42,
  c: false,
};

//console.log(Object.keys(text));
// expected output: Array ["a", "b", "c"]

function branch2(arg, callback1, callback2) {
  //if (Object.keys(text).indexOf(arg) != -1) {
  if (text[arg]) {
    callback1(text[arg]);
  } else {
    callback2("Ga ada guys");
  }
}

function callback1(arg) {
  console.log(arg);
}

function callback2(arg) {
  console.log(arg);
}

branch2("b", callback1, callback2);
branch2("cc", callback1, callback2);
