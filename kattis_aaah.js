const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let input = [];

rl.on("line", (line) => {
  input.push(line.length);
});

rl.on("close", () => {
  for (let i = 1; i < input.length; i++) {
    if (input[0] < input[i]) {
      console.log("no");
    } else {
      console.log("go");
    }
  }
});
