const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let input = [];

rl.on("line", (line) => {
  input.push(line);
});

rl.on("close", () => {
  input.reverse();
  input.pop();

  //console.log("AAAAAAA");

  while (input.length != 0) {
    //console.log("BBBBBBBBB");

    let soundFull = input.pop().split(" ");

    let line = input.pop();

    //console.log(line);

    while (line != "what does the fox say?") {
      //console.log("CCCCC");
      let sound = line.split(" ")[2];

      while (soundFull.indexOf(sound) != -1) {
        let index = soundFull.indexOf(sound);
        soundFull.splice(index, 1);
      }
      line = input.pop();
    }

    //console.log(soundFull);

    let res = [];

    for (let i = 0; i < soundFull.length; i++) {
      if (i != soundFull.length - 1) {
        res = res + soundFull[i] + " ";
      } else {
        res = res + soundFull[i];
      }
    }

    console.log(res);
  }
});
