const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let input = [];

rl.on("line", (line) => {
  input.push(line);
});

rl.on("close", () => {
  let lineSplit = input[0].split(" ");

  let h = lineSplit[0];
  let w = lineSplit[1];
  let brickNum = lineSplit[2];
  let brick = input[1].split(" ");
  let brickSum = 0;

  let hflag = 0;

  brick.forEach((element) => {
    brickSum += element;
  });

  if (brickSum < h * w) {
    console.log("NO");
  } else {
    let layer = w;
    for (let j = 0; j < brickNum; j++) {
      layer -= brick[j];

      //console.log("A", layer, "B", j, "C", brick[j]);

      if (layer == 0) {
        layer = w;
        hflag++;
        if (h == hflag) {
          console.log("YES");
          break;
        }
      }
      if (layer < 0) {
        console.log("NO");
        break;
      }
    }
  }

  /*
  for (let i = 0; i < h; i++) {
    let layer = w;
    for (; j < brickNum; j++) {
      layer -= brick[j];
    }
  }

  for (let i = 0; i < h; i++) {
    let layer = w;
    for (; j < brickNum; j++) {
      layer -= brick[j];
    }
  }

  */
});
