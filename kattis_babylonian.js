const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let input = [];

rl.on("line", (line) => {
  input.push(line);
});

rl.on("close", () => {
  input.reverse();
  input.pop();

  while (input.length > 0) {
    let line = input.pop();

    //console.log(line);

    let arr = line.split(",");
    let decimal = 0;

    for (let i = 0; i < arr.length; i++) {
      if (arr[i]) {
        //console.log(arr[i]);
        decimal += parseInt(arr[i]) * Math.pow(60, arr.length - i - 1);
      }
    }

    console.log(decimal);
  }
});
