const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let input = [];

rl.on("line", (line) => {
  input.push(line);
});

rl.on("close", () => {
  //console.log("A");

  //input = ["1", "Mandala", "2", "Jim", "Ben", "0"];

  input.reverse();

  //console.log("C", input);

  let flag = true;
  let set = 1;

  while (flag) {
    let num = input.pop();

    if (num == 0) {
      flag = false;
      break;
    }

    let name = [];

    for (let i = 0; i < num; i++) {
      name.push(input.pop());
    }

    name.reverse();

    //console.log("ZZZ", name);

    let asim = [];

    let firstIndex = 0;
    let lastIndex = name.length - 1;

    for (let i = 0; i < num; ) {
      asim[firstIndex] = name.pop();
      i++;
      firstIndex++;

      if (i == num) {
        break;
      }

      asim[lastIndex] = name.pop();
      i++;
      lastIndex--;
      //console.log(asim);
    }

    console.log("SET " + set);
    set++;

    asim.forEach((element) => {
      console.log(element);
    });
  }
});
