const http = require("http");
const net = require("net");
const date = new Date();

console.log(date.getUTCMonth());

let port = process.argv[2];

//onsole.log(port);

const server = net.createServer(function (socket) {
  socket.write(
    date.getFullYear() +
      "-" +
      (date.getMonth() + 1) +
      "-" +
      date.getDate() +
      " " +
      date.getHours() +
      ":" +
      date.getMinutes() +
      "\n"
  );

  socket.end();
});

server.listen(port);
