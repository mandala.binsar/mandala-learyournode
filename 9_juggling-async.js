const bl = require("bl");
const http = require("http");

//solution 1
//read url 1 until end and print it, after it finish read url 2 and so on
//

let url1 = process.argv[2];
let url2 = process.argv[3];
let url3 = process.argv[4];

http.get(url1, function (response) {
  response.setEncoding("utf8");
  let res = "";

  response.on("data", function (chuck) {
    res += chuck;
  });

  response.on("end", function () {
    console.log(res);

    http.get(url2, function (response) {
      response.setEncoding("utf8");
      let res = "";

      response.on("data", function (chuck) {
        res += chuck;
      });

      response.on("end", function () {
        console.log(res);

        http.get(url3, function (response) {
          response.setEncoding("utf8");
          let res = "";

          response.on("data", function (chuck) {
            res += chuck;
          });

          response.on("end", function () {
            console.log(res);
          });
        });
      });
    });
  });
});

//solution 2 (still false)
/*
let urls = [process.argv[2], process.argv[3], process.argv[4]];
let result = [];
let count = 0;

for (let i = 0; i < urls.length; i++) {
  http.get(urls[i], function (response) {
    response.setEncoding("utf8");
    response.pipe(
      bl(function (err, data) {
        result[i] = data.toString();
        count++;

        if (count === urls.length) {
          result.forEach((element) => console.log(element));
          //console.log("AAA", result);
        }
      })
    );
  });
}
*/
