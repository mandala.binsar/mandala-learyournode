const artist = [
  { name: "AAA", age: 29 },
  { name: "BBB", age: 33 },
  { name: "CCc", age: 29 },
  { name: "DdD", age: 2 },
  { name: "eEE", age: 11 },
  { name: "FfF", age: 7 },
];

function mapMe(someArray, someFunction) {
  let result = [];

  for (let i = 0; i < someArray.length; i++) {
    result[result.length] = someFunction(someArray[i]);
  }
  return result;
}

let result2 = mapMe(artist, function (artis) {
  return "The artist called " + artis.name + " is " + artis.age + " years old";
});

function filterMe(someArray, someFunction) {
  let result = [];

  for (let i = 0; i < someArray.length; i++) {
    if (someFunction(someArray[i])) {
      result[result.length] = someArray[i];
    }
  }
  return result;
}

let result3 = filterMe(artist, function (artis) {
  return artis.age < 21;
});

console.log(result3);

let ganjil = artist.filter(function (element, index, oriArray) {
  return index % 2 === 1;
});

console.log(ganjil);

/*
let kk = ["AAA", "BBB", "CCCC"];

let cc = kk.reduce(function (res, next) {
  return (res += next);
});

console.log(cc);
*/

// modify code below
/*
let result = artist.map(
  (art) => "The artist called " + art.name + " is " + art.age + " years old"
);

result.forEach((element) => {
  console.log(element);
});
*/

//mapMe()

[
  // "The artist called NAME is AGE years old",
  // "The artist called NAME is AGE years old",
  // "The artist called NAME is AGE years old",
  // "The artist called NAME is AGE years old",
];
// modify code above

/*
let i = [1, 2, 3, 4, 5, 6, "what does the fox say?"];

if (i.pop() == "what does the fox say?") {
  console.log("AAAAAAAAAAAAAAAAAAAAAA");
}

let kk = ["AAA", "BBB", "CCCC"];

let cc = kk.reduce(function (res, next) {
  console.log(res, " --- ", next);

  res += next;
});

console.log(cc);

console.log(i);
i.reverse();
console.log(i);
let a = i.pop();
console.log(i.pop());
console.log(i.pop());
console.log(i.pop());
console.log(i.pop());
console.log(i.pop());
console.log(i.pop());
console.log(i.pop());
console.log(a);

if (i.pop()) {
  console.log("AAAA");
} else {
  console.log("BBB");
}
*/
