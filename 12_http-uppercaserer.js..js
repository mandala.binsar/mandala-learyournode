const http = require("http");
const fs = require("fs");
const map = require("through2-map");
const bl = require("bl");

let port = process.argv[2];
let dir = process.argv[3];

const server = http.createServer((request, response) => {
  request
    .pipe(
      map(function (chunk) {
        return chunk.toString().toUpperCase();
      })
    )
    .pipe(response);
});

server.listen(port);
