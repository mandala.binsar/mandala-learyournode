function mainOld(callback1, callback2) {
  setTimeout(function () {
    callback1();
  }, 2000);

  let interval = setInterval(function () {
    callback2();
  }, 1000);

  setTimeout(function () {
    clearInterval(interval);
  }, 3100);
}

function helloWorld() {
  console.log("hello world");
}

function helloTelkomsel() {
  console.log("hello telkomsel");
}

//main(helloWorld, helloTelkomsel);

function main(callback1, callback2) {
  setTimeout(callback1, 2000);

  let interval = setInterval(callback2, 1000);

  setTimeout(function () {
    clearInterval(interval);
  }, 3100);
}

function hoc() {
  return function () {
    console.log("hello worldddd");
  };
}

let a = hoc();
//a();

//or

//hoc()();

function multiplyWith(a) {
  return function (b) {
    return a * b;
  };
}

let triple = multiplyWith(3);
console.log(triple(2));

console.log(multiplyWith(3)(2));
